import VF as vf
import scipy as sp
from sklearn import svm
import numpy as np
import csv

from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_multilabel_classification
from sklearn.multioutput import MultiOutputRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import KNeighborsRegressor

from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPRegressor
from sklearn.linear_model import SGDRegressor
from sklearn.linear_model import Ridge

import matplotlib.pyplot as plt


bs_coordinates, traces = vf.get_data()
# Get every time-step -> from 3845 to 86308 -> about 22.9 hours
times = vf.compute_uniq_times(traces)

#vf.BS_load_plot(bs_coordinates, traces, times)

X, Y = vf.pre_processing(traces, times, save=True)

'''
X = np.asarray(X)
Y = np.asarray(Y)
permutation = np.random.permutation(X.shape[0])

X = X[permutation]
Y = Y[permutation]

m_training = 7500

X_train, X_test = X[:m_training], X[m_training:]
Y_train, Y_test = Y[:m_training], Y[m_training:]

print('')
'''

'''
X = []
Y = []

with open('pos_features.csv', 'r') as file:
    reader = csv.reader(file, delimiter=',')

    for row in reader:
        X.append(np.asarray([float(row[0]), float(row[1]), float(row[2])]))


with open('pos_labels.csv', 'r') as file:
    reader = csv.reader(file, delimiter=',')

    for row in reader:
        Y.append(np.asarray([float(row[0]), float(row[1])]))


X = np.asarray(X)
Y = np.asarray(Y)

m_training = 2000

X_train, X_test = X[:m_training], X[m_training:]
Y_train, Y_test = Y[:m_training], Y[m_training:]


###### -------------------------------------------------------------------------------------    Test multiregression

max_depth = 1000
#model = MultiOutputRegressor(RandomForestRegressor(n_estimators=100, max_depth=max_depth, random_state=0))
#knn = KNeighborsRegressor()
ridge = Ridge()
model = MultiOutputRegressor(ridge)
model.fit(X_train, Y_train)

pred_y = model.predict(X_test)

error = np.zeros(len(Y_test))
for i in range(len(Y_test)):
    print('True pos:', Y_test[i])
    print('Pred pos:', pred_y[i])
    error[i] = np.linalg.norm(Y_test[i] - pred_y[i])


print('ERROR')
print(error)

plt.figure()
plt.plot(error)
plt.show()
# Show figure


plt.figure()
s = 10
a = 0.4
plt.show(block=False)
for i in range(len(Y_test)):
    # Plot current position
    cur_pos = plt.scatter(X_test[i, 0], X_test[i, 1], c="orange", s=s, marker="s", alpha=1, label="Data")
    # Plot true next position
    true_pos = plt.scatter(Y_test[i, 0], Y_test[i, 1], c="navy", s=s, marker="s", alpha=1, label="Data")
    # Plot predicted position
    pred_pos = plt.scatter(pred_y[i, 0], pred_y[i, 1], edgecolor='k', c="r", s=s, alpha=a)

    plt.draw()
    plt.pause(0.5)

    true_pos.remove()
    pred_pos.remove()
    cur_pos.remove()
'''



### -------------------------------------------------------------------------------------------   Test SGDRegressor

#%%





#%%

'''
import warnings
warnings.filterwarnings('ignore')

#parameters = {'hidden_layer_sizes': [(10,), (20,), (40,), (40,20,), (40,30,20), (100, 10)]}
nn_model = MLPRegressor(max_iter=300, hidden_layer_sizes=(5, 2, 4, 1), alpha=1e-4, solver='sgd', tol=1e-4, learning_rate_init=.1)

#search_1 = GridSearchCV(nn_model, parameters, cv=5)
#search_1.fit(X_train, Y_train)

nn_model.fit(X_train, Y_train)

#print("Loss:")
#print(nn_model.loss_, '\n')

pred_y = nn_model.predict(X_test)

error = np.zeros(len(Y_test))
for i in range(len(Y_test)):
    #print('True pos:', Y_test[i])
    #print('Pred pos:', pred_y[i])
    error[i] = np.linalg.norm(Y_test[i] - pred_y[i])


print('ERROR')
print(error)

plt.figure()
plt.plot(error)
plt.show()
'''

